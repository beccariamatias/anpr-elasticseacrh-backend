package com.zinbig.com.zinbig.danaide.anpr.elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * @author Mat�as Beccaria
 *
 */
@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.zinbig.com.zinbig.danaide.anpr.elastic.repositories")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
