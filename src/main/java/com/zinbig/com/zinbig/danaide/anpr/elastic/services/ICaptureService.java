package com.zinbig.com.zinbig.danaide.anpr.elastic.services;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.zinbig.com.zinbig.danaide.anpr.elastic.domain.Capture;

/**
 * @author Mat�as Beccaria
 *
 */
public interface ICaptureService {

	List<Capture> findByLikePlate(String regExpPlate) throws InterruptedException, ExecutionException;

	Iterable<Capture> findAll();
}
