package com.zinbig.com.zinbig.danaide.anpr.elastic.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.concurrent.ListenableFuture;

import com.zinbig.com.zinbig.danaide.anpr.elastic.domain.Capture;

/**
 * @author Mat�as Beccaria
 *
 */
public interface CaptureRepository extends ElasticsearchRepository<Capture, String> {

	@Async
	ListenableFuture<List<Capture>> findByPlatestring(String platestring, Pageable pageable);
}