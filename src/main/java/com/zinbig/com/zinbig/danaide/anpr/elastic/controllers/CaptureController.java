package com.zinbig.com.zinbig.danaide.anpr.elastic.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zinbig.com.zinbig.danaide.anpr.elastic.domain.Capture;
import com.zinbig.com.zinbig.danaide.anpr.elastic.services.ICaptureService;

/**
 * @author Mat�as Beccaria
 *
 */
@RestController
@RequestMapping("/captures")
public class CaptureController {

	@Autowired
	private ICaptureService captureService;

	@GetMapping(value = "/plates/{regExpPlate}")
	public ResponseEntity<Iterable<Capture>> search(@PathVariable("regExpPlate") final String regExpPlate)
			throws Exception {
		Iterable<Capture> result = getCaptureService().findByLikePlate(regExpPlate);
		return ResponseEntity.ok(result);
	}

	@GetMapping(value = "/plates/")
	public ResponseEntity<Iterable<Capture>> findAll() {
		Iterable<Capture> result = getCaptureService().findAll();
		return ResponseEntity.ok(result);
	}

	private ICaptureService getCaptureService() {
		return captureService;
	}

}
