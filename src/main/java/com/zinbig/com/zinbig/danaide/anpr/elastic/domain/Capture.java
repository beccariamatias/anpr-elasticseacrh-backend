package com.zinbig.com.zinbig.danaide.anpr.elastic.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author Mat�as Beccaria
 *
 */
@Document(indexName = "captures", type = "capture", shards = 5, replicas = 1)
public class Capture {

	@Id
	private Long idcapture;

	private String platestring;

	public Long getIdcapture() {
		return idcapture;
	}

	public void setIdcapture(Long idcapture) {
		this.idcapture = idcapture;
	}

	public String getPlatestring() {
		return platestring;
	}

	public void setPlatestring(String platestring) {
		this.platestring = platestring;
	}

}
