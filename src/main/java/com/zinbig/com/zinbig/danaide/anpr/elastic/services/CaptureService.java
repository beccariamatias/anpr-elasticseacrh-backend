package com.zinbig.com.zinbig.danaide.anpr.elastic.services;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.zinbig.com.zinbig.danaide.anpr.elastic.domain.Capture;
import com.zinbig.com.zinbig.danaide.anpr.elastic.repositories.CaptureRepository;

/**
 * @author Mat�as Beccaria
 *
 */
@Service
public class CaptureService implements ICaptureService {

	@Autowired
	private CaptureRepository captureRepository;

	@Override
	public List<Capture> findByLikePlate(String regExpPlate) throws InterruptedException, ExecutionException {
		// index.max_result_window = 10000 en este caso
		// https://www.elastic.co/guide/en/elasticsearch/guide/current/_fetch_phase.html
		ListenableFuture<List<Capture>> findByPlatestring = getCaptureRepository().findByPlatestring(regExpPlate,
				PageRequest.of(0, 10000));
		return findByPlatestring.get();
	}

	@Override
	public Iterable<Capture> findAll() {
		// index.max_result_window = 10000 en este caso
		// https://www.elastic.co/guide/en/elasticsearch/guide/current/_fetch_phase.html
		return getCaptureRepository().findAll(PageRequest.of(0, 10000));
	}

	private CaptureRepository getCaptureRepository() {
		return captureRepository;
	}
}
