# README #

Prototipo con Spring Boot 2 y spring-data-elasticsearch para probar b�squedas con wildcard sobre patentes.

Ejecutar la clase Application.java para levantar el backend con Spring Boot 2.

Ejemplo invocaci�n via REST: http://localhost:8080/captures/plates/NIG*  --> (Si no se agrega el * busca por palabra exacta)

Para configurar la URL de ES o el nombre del cluster: src/main/resources/application.properties 

* Referencias
	- https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/
	- https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#elasticsearch.query-methods

